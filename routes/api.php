<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function() {
    Route::post('/register', 'RegisterController');
    Route::post('/login', 'LoginController');
    Route::post('/logout', 'LogoutController');
});

Route::middleware('auth:api')->group(function() {
    Route::get('/home', 'BlogController@index');
    Route::post('/blog/create', 'BlogController@store');
    Route::get('/{slug}', 'BlogController@show');
    // Route::patch('/{id}/update', 'BlogController@update');
    // Route::delete('/{id}/delete', 'BlogController@destroy');
});

Route::middleware('auth:api', 'editor')->group(function() {
    Route::get('/review', 'EditorController@index');
    Route::get('/review/{id}', 'EditorController@show');
    Route::post('/review/{id}/approved', 'EditorController@approve');
});

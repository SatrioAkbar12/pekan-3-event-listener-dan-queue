<?php

namespace App\Http\Controllers;

use App\Http\Resources\BlogResource;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Blog;
use App\Events\NewBlogEvent;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Blog::all();

        return BlogResource::collection(($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        $slug = Str::of($request->judul)->slug('-');

        $data = Blog::create([
            'user_id' => auth()->user()->id,
            'judul' => $request->judul,
            'isi' => $request->isi,
            'slug' => $slug,
        ]);

        $data_event = [
            'user' => auth()->user(),
            'blogTitle' => $request->judul
        ];

        event(new NewBlogEvent($data_event));

        return response()->json([
            'status' => 'saved',
            'data' => $data,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data = Blog::where('slug', $slug)->get();

        return new BlogResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'nama' => ['required'],
            'email' => ['email', 'required', 'unique:users,email'],
            'password' => ['required']
        ]);

        $data = User::create([
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User berhasil didaftakan',
            'data' => $data
        ], 200);
    }
}

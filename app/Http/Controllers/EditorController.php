<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\ApprovedBlogEvent;
use App\Models\Blog;
use App\User;
use App\Http\Resources\BlogResource;

class EditorController extends Controller
{
    public function index(){
        $data = Blog::where('status_publish', 0)->get();

        return BlogResource::collection($data);
    }

    public function show($id){
        $data = Blog::where('id', $id)->get();

        return new BlogResource($data);
    }

    public function approve($id){
        Blog::where('id', $id)->update([
            'status_publish' => 1
        ]);

        $data = Blog::where('id', $id)->get();
        $user = User::where('id', $data[0]->user_id)->get();

        $data_event = [
            'user' => $user[0],
            'blogTitle' => $data[0]->judul
        ];

        event(new ApprovedBlogEvent($data_event));

        return response()->json([
            'status' => 'Published'
        ]);
    }
}

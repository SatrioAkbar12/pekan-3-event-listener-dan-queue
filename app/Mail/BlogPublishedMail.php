<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BlogPublishedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $blogTitle;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $blogTitle)
    {
        return [
            $this->user = $user,
            $this->blogTitle = $blogTitle
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('nama_website@website.com')
                    ->view('mails.blog_published')
                    ->with([
                        'userName' => $this->user,
                        'blogTitle' => $this->blogTitle
                    ]);
    }
}

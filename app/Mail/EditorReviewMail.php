<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EditorReviewMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $editor;
    protected $blogTitle;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($editor, $blogTitle)
    {
        return [
            $this->editor = $editor,
            $this->blogTitle = $blogTitle
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->from('nama_website@website.com')
                    ->view('mails.editor_review')
                    ->with([
                        'editorName' => $this->editor,
                        'blogTitle' => $this->blogTitle
                    ]);
    }
}

<?php

namespace App\Listeners;

use App\Events\NewBlogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserBlogReviewMail;

class SendNotificationMailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBlogEvent  $event
     * @return void
     */
    public function handle(NewBlogEvent $event)
    {
        Mail::to($event->data['user']['email'])->send(new UserBlogReviewMail($event->data['user']['name'], $event->data['blogTitle']));
    }
}

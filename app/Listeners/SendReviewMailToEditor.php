<?php

namespace App\Listeners;

use App\Events\NewBlogEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\EditorReviewMail;
use App\User;

class SendReviewMailToEditor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBlogEvent  $event
     * @return void
     */
    public function handle(NewBlogEvent $event)
    {
        $editor = User::where('role_id', 1)->get();

        foreach($editor as $editor){
            Mail::to($editor->email)->send(new EditorReviewMail($editor->name, $event->data['blogTitle']));
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Blog extends Model
{
    protected $fillable = [
        'user_id', 'judul', 'isi', 'slug', 'status_publish'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}

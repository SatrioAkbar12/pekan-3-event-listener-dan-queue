<!DOCTYPE html>
<html>
    <head>
        <title>Artikel Baru</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h1 class="display-4 text-center">Nama Website</h1>

            <p>Hai Editor {{ $editorName }}!</p>
            <p>Kami ingin memberitahukan bahwa ada artikel baru dengan judul <span style="color:dodgerblue;">{{ $blogTitle }}</span> yang baru saja dibuat.</p>
            <p>Kami ingin anda untuk mereview artikel ini agar artikel ini bisa di publish</p>

            <h4 class="text-center">Terima Kasih!</h4>

        </div>
    </body>
</html>

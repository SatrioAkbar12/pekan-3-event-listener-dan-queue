<!DOCTYPE html>
<html>
    <head>
        <title>Blog Sudah Di Publish</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h1 class="display-4 text-center">Nama Website</h1>

            <p>Selamat saudara {{ $userName }}!</p>
            <p>Blog Anda yang berjudul <span style="color:dodgerblue;">{{ $blogTitle }}</span> sudah di-review dan sudah dipublish</p>

            <h4 class="text-center">Terima Kasih!</h4>

        </div>
    </body>
</html>
